import sendEmail from "../../src/api/sendEmail";

test("Create User Test", () => {
  // test create user
  // param: (emailAddress, password)
  // returns statusCode
});

test("Sign User In", () => {
  // test to authenticate a user
  // param: (emailAddress, password)
  // returns: authToken, statusCode
});

test("Send Signup Email", () => {
  // test send email
  // param: emailAddress
  // returns: statusCode

  sendEmail();
});

test("Create PriceWatch Subscription", () => {
  // test to create a PriceWatch subscription
  // param: (cryptoSymbol, cryptoPurchasePrice)
  // returns: statusCode
});

test("Check Crypto Prices", () => {
  // test to check the crypto prices for a given token
  // param: cryptoSymbol
  // returns: currentUnitPrice, statusCode
});

test("Send PriceAlert Email", () => {
  // test send email
  // param: emailAddress
  // returns: statusCode
});
