import { mandrillAPIKey } from "../keys/mandrillSecretKey";

var mandrill = require("node-mandrill")(mandrillAPIKey);

function sendEmail() {
  //send an e-mail to jim rubenstein
  mandrill(
    "/messages/send",
    {
      message: {
        to: [{ email: "tlinkey0818@gmail.com", name: "Travis Linkey" }],
        from_email: "crypto-trading-alert@bot.net",
        subject: "Hey, what's up?",
        text: "Hello, I sent this message using mandrill.",
      },
    },
    function (error, response) {
      //uh oh, there was an error
      if (error) console.log(JSON.stringify(error));
      //everything's good, lets see what mandrill said
      else console.log(response);
      console.log("Sending an email!");
    }
  );
}

export default sendEmail;
